from mocked_apis.models import ApiModelState


class ApiObject(object):
    def __init__(self, serializer=None, pk=None, **kwargs):
        self.pk = pk
        self._serializer = serializer
        for field, value in kwargs.iteritems():
            setattr(self, field, value)

    def save(self):
        fields = self._serializer(self).data
        fields["pk"] = self.pk
        ApiModelState.objects.filter(pk=fields.get("pk")).update(fields=fields)