"""api_mocker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import importlib
import pkgutil

from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.routers import Route

from mocked_apis.viewsets import BaseViewSet


class FixedRouter(routers.SimpleRouter):
    routes = [
        # List route.
        Route(
            url=r'^{prefix}{trailing_slash}$',
            mapping={
                'get': 'list',
                'post': 'create'
            },
            name='{basename}-list',
            initkwargs={'suffix': 'List'}
        ),
        # Detail route.
        Route(
            url=r'^{prefix}/{lookup}{trailing_slash}$',
            mapping={
                'get': 'retrieve',
                'put': 'update',
                'patch': 'partial_update',
                'delete': 'destroy'
            },
            name='{basename}-detail',
            initkwargs={'suffix': 'Instance'}
        ),
        Route(
            url=r'^{prefix}/{lookup}/(?P<method_name>[\w-]+){trailing_slash}$',
            mapping={
                'post': 'detail_method_handler',
            },
            name='{basename}-detail-attr',
            initkwargs={'suffix': 'Instance'}
        ),
    ]


def generate_url_patterns():
    _urlpatterns = []
    for _, api_module_name, _o in pkgutil.iter_modules(['mocked_apis/api_conf']):
        conf_module = importlib.import_module(".{}".format(api_module_name), package=("mocked_apis.api_conf"))
        router = FixedRouter()
        for endpoint in conf_module.endpoints.iterkeys():
            router.register(endpoint, BaseViewSet, base_name=endpoint)
        _urlpatterns.append(url(r'^{}/'.format(conf_module.API_NAME),
                                include(router.urls, namespace=conf_module.API_NAME),
                                kwargs={"conf_module": conf_module}))

    return _urlpatterns

urlpatterns = generate_url_patterns()
