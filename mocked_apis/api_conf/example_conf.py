from decimal import Decimal

from rest_framework import serializers

from mocked_apis.serializers import BaseSerializer

# API_NAME is also base endpoint of api - use slug
API_NAME = 'api_name_example'

endpoints = {
    r'account': {
        'detail_methods': ('product_buy', 'top_up'),
        'serializer': 'AccountSerializer',
    }
}


def product_buy(request, account):
    try:
        price = Decimal(request.POST.get('price'))
        if price <= account.cash:
            account.cash -= price
            account.save()
            code, message = 200, "success"
        else:
            code, message = 400, "Too expensive"
    except TypeError:
        code, message = 400, "Invalid price"
    return code, message


def top_up(request, account):
    try:
        amount = Decimal(request.POST.get('amount'))
        account.cash += amount
        account.save()
        code, message = 200, "success"
    except TypeError:
        code, message = 400, "Invalid price"
    return code, message


class AccountSerializer(BaseSerializer):
    cash = serializers.DecimalField(max_digits=5, decimal_places=2)
    transfer = serializers.IntegerField()
