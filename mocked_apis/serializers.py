from rest_framework import serializers

from mocked_apis.utils import ApiObject


class BaseSerializer(serializers.Serializer):

    def create(self, pk=None, validated_data=None):
        if not validated_data:
            if self.is_valid():
                validated_data = self.validated_data
            else:
                validated_data = {}
        validated_data["pk"] = pk
        return ApiObject(serializer=self.__class__, **validated_data)

    def update(self, instance, validated_data):
        for key, value in validated_data.iteritems():
            setattr(instance, key, value)
        return instance
