import logging

import requests
from rest_framework import viewsets
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response

from mocked_apis.models import ApiClient, ApiLog, ApiModelState, ApiModel, MockedApi


class BaseViewSet(viewsets.ModelViewSet):
    lookup_field = 'pk'
    lookup_url_kwarg = 'pk'

    def __init__(self, **kwargs):
        super(BaseViewSet, self).__init__(**kwargs)
        self.client = None
        self.callback_result = False
        self.status = ""
        self.api_conf = None
        self.api_model = None
        self.api = None

    def dispatch(self, request, conf_module=None, *args, **kwargs):
        self.setup_view(request, conf_module)
        response = super(BaseViewSet, self).dispatch(request, *args, **kwargs)
        self.send_callback(self.request.path)
        self.save_log(request, response)

        return response

    def setup_view(self, request, conf_module):
        self.client, created = ApiClient.objects.get_or_create(pk=self.get_client_id(request))
        self.api_conf = conf_module
        self.api, created = MockedApi.objects.get_or_create(name=self.api_conf.API_NAME)
        self.api_model, created = ApiModel.objects.get_or_create(
            endpoint=request.path.split(self.api_conf.API_NAME)[-1].strip('/').split('/', 1)[0], api=self.api)

    def save_log(self, request, response):
        ApiLog.objects.create(call_endpoint=request.path, call_method=request.method, call_body=self.request.data,
                              client=self.client, response_status=response.status_code,
                              response_data=response.data, callback_success=self.callback_result)

    def get_serializer_class(self):
        serializer_name = self.api_conf.endpoints.get(self.api_model.endpoint, {}).get('serializer')
        return getattr(self.api_conf, serializer_name)

    def send_callback(self, path):
        if self.client.callback_url:
            try:
                resp = requests.post(self.client.callback_url, data={'endpoint': path, 'status': self.status})
                if resp.ok:
                    self.callback_result = True
            except Exception as ex:
                logging.error("{}: Callback failed: {}".format(ex, self.client.callback_url))

    def get_queryset(self):
        return ApiModelState.objects.filter(api_model=self.api_model, api_client=self.client)

    def get_object(self):
        api_state = super(BaseViewSet, self).get_object()
        serializer = self.get_serializer_class()(data=api_state.fields)
        return serializer.create(pk=api_state.pk)

    def list(self, request, *args, **kwargs):
        return Response(self.get_queryset().values_list('fields', flat=True))

    @staticmethod
    def get_client_id(request):
        try:
            client_id = int(getattr(request, request.method, {}).get("CLIENT_ID"))
        except (TypeError, ValueError) as ex:
            client_id = 1
            logging.error("Invalid header CLIENT_ID")
        return client_id

    def perform_create(self, serializer):
        api_state = ApiModelState.objects.create(api_model=self.api_model, api_client=self.client)
        fields = serializer.data
        fields.update({'pk': api_state.pk})
        api_state.fields = fields
        api_state.save()

    def detail_method_handler(self, request, pk=None, method_name=''):
        handler = getattr(self.api_conf, method_name)
        if method_name in self.api_conf.endpoints.get(self.api_model.endpoint, {}).get('detail_methods') \
                and callable(handler):
            code, message = handler(request, self.get_object())
            return Response(status=code, data={"status": message})
        else:
            raise MethodNotAllowed()
