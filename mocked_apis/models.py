from __future__ import unicode_literals

from django.contrib.postgres.fields import HStoreField
from django.db import models

# Create your models here.


class MockedApi(models.Model):
    name = models.CharField(max_length=255)
    active = models.BooleanField(default=True)


class ApiClient(models.Model):
    callback_url = models.URLField(null=True, blank=True)


class ApiLog(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    call_endpoint = models.CharField(max_length=255)
    call_method = models.CharField(max_length=16)
    call_body = models.TextField(null=True, blank=True)
    client = models.ForeignKey(ApiClient, related_name="api_logs")
    response_status = models.IntegerField(default=200)
    response_data = models.TextField(null=True, blank=True)
    callback_success = models.BooleanField(default=True)


class ApiModel(models.Model):
    endpoint = models.CharField(max_length=255)
    api = models.ForeignKey(MockedApi)

    class Meta:
        unique_together = ('endpoint', 'api')


class ApiModelState(models.Model):
    updated = models.DateTimeField(auto_now=True)
    api_model = models.ForeignKey(ApiModel, related_name="fields")
    api_client = models.ForeignKey(ApiClient)
    fields = HStoreField(null=True, blank=True)


