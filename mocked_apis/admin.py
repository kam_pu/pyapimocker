from django.contrib import admin
from mocked_apis import models

# Register your models here.

admin.site.register(models.ApiModelState)
admin.site.register(models.ApiClient)
admin.site.register(models.ApiLog)
admin.site.register(models.MockedApi)